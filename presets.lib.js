const fs = require('fs');
const path = require('path');
const crypto = require('crypto');
const json5 = require('json5');

class _slavePreset {
	constructor() {
		const self = this;

		self._processed = [];
		self._list = {};
	}

	/**
	 * Get hash value of a content
	 * @private
	 * @param {string} content - input string
	 * @param {string} [algo='sha512'] - algorithm used to get the hash
	 * @returns {string} hash in hexadecimal string form
	 * */
	_hash(content, algo = 'sha512') {
		return crypto.createHash(algo)
			.update(content)
			.digest('hex');
	}

	/**
	 * Get all json files from a directory recursively
	 * @private
	 * @param {string} dirpath - directory path
	 * @returns {string[]} array of json file paths
	 * */
	_getAllFiles(dirpath){
		const self = this;
		const files = [];

		for(const item of fs.readdirSync(dirpath)){
			const pathname = `${dirpath}/${item}`;

			if(!fs.statSync(pathname).isDirectory()){
				if(!pathname.match(/\.json$|\.json5$/)) continue;
				files.push(pathname);
				continue;
			}

			files.push(...self._getAllFiles(pathname));
		}

		return files;
	}

	/**
	 * Convert number ito hexadecimal string
	 * @private
	 * @param {number} val - number to convert
	 * @returns {string} hexadecimal string of value
	 * */
	_toHex(val){
		return isNaN(val) ? '' : `${val.toString(16)}`;
	}

	/**
	 * Cache allowed item inside JSON object inside list.
	 * List is used to search allowed items identified by slave's position,
	 * vendor id, and/or product code.
	 * @private
	 * @param {object} obj - JSON object from file
	 * */
	_singleItem(obj){
		const self = this;

		const key = {
				position: '',
				vendor_id: '',
				product_code: '',
			};

		if(obj.position != undefined){
			key.position = self._toHex(Number(obj.position));
		}

		if(obj.vendor_id != undefined){
			key.product_code = self._toHex(Number(obj.vendor_id));
		}

		if(obj.product_code != undefined){
			key.product_code = self._toHex(Number(obj.product_code));
		}

		const id = `${key.position}:${key.vendor_id}:${key.product_code}`;

		if(self._list[id] !== undefined){
			console.error(key, ' already exists!');
			return false;
		}

		const allowed = [
				'parameters',
				'name',
			];

		const res = {};

		for(const item of allowed){
			if(obj[item] !== undefined) res[item] = obj[item];
		}

		self._list[id] = res;
	}

	/**
	 * Parse json file into object
	 * @private
	 * @param {string} string - stringified JSON object
	 * */
	_toObject(string) {
		const self = this;

		// stop if file is already processsed
		const hash = self._hash(string);
		if(self._processed.includes(hash)) return;

		self._processed.push(hash);

		const obj = json5.parse(string);

		if(!Array.isArray(obj)) {
			self._singleItem(obj);
			return;
		}

		for(const item of obj){
			self._singleItem(item);
		}
	}

	/**
	 * Get content of a json file
	 * @private
	 * @param {string} filename - path to json file
	 * */
	_getContents(filename) {
		const self = this;

		try {
			const contents = fs.readFileSync(filename, { encoding: 'utf8' });
			self._toObject(contents);
		} catch(error) {
			console.error(`Invalid presets file '${filename}'`);
		}
	}

	/**
	 * Set list from json file path. If directory path is passed, then it will
	 * try to find all json files inside it recursively.
	 * @param {string} pathname - path to json file or directory containing json files
	 * */
	setPath(pathname){
		const self = this;

		if(!fs.existsSync(pathname)) {
			console.error(`'${pathname}' doesn't exist!`);
			return;
		}

		if(!fs.statSync(pathname).isDirectory()){
			self._getContents(pathname);
			return;
		}

		for(const file of self._getAllFiles(pathname)){
			self._getContents(file);
		}
	}

	/**
	 * Find item from processed JSON file. Item is identified by following combinations:
	 *   - position
	 *   - product code
	 *   - position & product code
	 *   - position, vendor id, & product code
	 *   - vendor id & product code
	 * @param {object} obj
	 * @param {number|string} [obj.position] - slave's position
	 * @param {number|string} [obj.vendor_id] - slave's vendor id
	 * @param {number|string} [obj.product_code] - slave's product code
	 * @returns {object} allowed item or empty object if not found
	 * */
	find({ position, vendor_id, product_code }){
		const self = this;

		position = self._toHex(Number(position));
		vendor_id = self._toHex(Number(vendor_id));
		product_code = self._toHex(Number(product_code));

		const combinations = [
				`${position}::`,
				`${position}:${vendor_id}:${product_code}`,
				`${position}::${product_code}`,
				`:${vendor_id}:${product_code}`,
				`::${product_code}`,
			];

		for(const id of combinations){
			if(self._list[id]) return self._list[id];
		}

		return {};
	}

	get list(){
		return this._list;
	}
}

module.exports = new _slavePreset();
