{
	"$schema": "http://json-schema.org/draft-07/schema",
	"$id": "https://gitlab.com/iqrok/ethercat-cstruct-converter/-/raw/master/slaves-configuration.schema.json?ref_type=heads",
	"type": "array",
	"title": "SlavesConfiguration",
	"description": "All attached slaves must be defined in here.",
	"$defs": {
		"hexadecimal": {
			"anyOf": [
				{
					"type": "integer",
					"minimum": 0
				},
				{
					"type": "string",
					"pattern": "^0x[0-9a-fA-F]+"
				}
			]
		}
	},
	"items": {
		"type": "object",
		"title": "Slave",
		"additionalProperties": false,
		"required": [
			"alias",
			"position",
			"vendor_id",
			"product_code"
		],
		"properties": {
			"alias": {
				"$ref": "#/$defs/hexadecimal",
				"description": "Slave's alias number (in integer or hexadecimal string).",
				"examples": [
					0
				]
			},
			"position": {
				"$ref": "#/$defs/hexadecimal",
				"description": "Slave's position relative to master (in integer or hexadecimal string).",
				"examples": [
					0,
					1
				]
			},
			"vendor_id": {
				"$ref": "#/$defs/hexadecimal",
				"description": "Slave's vendor id (in integer or hexadecimal string).",
				"examples": [
					"0x00000002",
					2
				]
			},
			"product_code": {
				"$ref": "#/$defs/hexadecimal",
				"description": "Slave's product code (in integer or hexadecimal string).",
				"examples": [
					"0x0fa43052",
					262418514
				]
			},
			"revision": {
				"$ref": "#/$defs/hexadecimal",
				"description": "Slave's revision number (in integer or hexadecimal string).",
				"examples": [
					"0x00100000",
					1048576
				]
			},
			"serial": {
				"$ref": "#/$defs/hexadecimal",
				"description": "Slave's serial number (in integer or hexadecimal string).",
				"examples": [
					"0x00000000",
					0
				]
			},
			"name": {
				"type": "string",
				"description": "Slave device's name.",
				"examples": [
					"EL4004",
					"EK1100"
				]
			},
			"syncs": {
				"type": "array",
				"title": "syncs",
				"description": "SM configuration. Omit this field if the slave is a bus coupler, such as EK1100",
				"items": {
					"type": "object",
					"title": "SyncManager",
					"required": [
						"index",
						"pdos"
					],
					"examples": [
						{
							"index": 2,
							"watchdog_enabled": false,
							"pdos": [
								{
									"index": "0x1600"
								},
								{
									"index": "0x1601"
								},
								{
									"index": "0x1602"
								},
								{
									"index": "0x1603"
								}
							]
						}
					],
					"properties": {
						"index": {
							"type": "integer",
							"minimum": 0,
							"description": "Sync Manager index"
						},
						"watchdog_enabled": {
							"type": "boolean",
							"description": "Watchdog status. If omitted, then it would be treated as false.",
							"default": false
						},
						"pdos": {
							"type": "array",
							"title": "pdos",
							"description": "PDO entries.",
							"items": {
								"type": "object",
								"title": "PDOEntry",
								"examples": [
									{
										"index": "0x1600"
									},
									{
										"index": "0x1a00",
										"entries": [
											{
												"index": "0x6000",
												"subindex": "0x01",
												"size": 16,
												"add_to_domain": true,
												"swap_endian": true,
												"signed": false
											}
										]
									}
								],
								"required": [
									"index"
								],
								"properties": {
									"index": {
										"$ref": "#/$defs/hexadecimal",
										"description": "PDO CoE index (in integer or hexadecimal string)."
									},
									"entries": {
										"type": "array",
										"title": "sdos",
										"description": "Map PDO from SDO entries.",
										"items": {
											"type": "object",
											"title": "SDOEntry",
											"examples": [
												{
													"index": "0x6000",
													"subindex": "0x01",
													"size": 16,
													"add_to_domain": true,
													"swap_endian": true,
													"signed": false
												}
											],
											"required": [
												"index",
												"subindex",
												"size"
											],
											"properties": {
												"index": {
													"$ref": "#/$defs/hexadecimal",
													"description": "SDO CoE index to be mapped to PDO (in integer or hexadecimal string)."
												},
												"subindex": {
													"$ref": "#/$defs/hexadecimal",
													"description": "SDO CoE subindex to be mapped to PDO (in integer or hexadecimal string)."
												},
												"size": {
													"$ref": "#/$defs/hexadecimal",
													"description": "Size in bit (in integer or hexadecimal string)."
												},
												"add_to_domain": {
													"type": "boolean",
													"description": "Add to Domain or not.",
													"default": false
												},
												"swap_endian": {
													"type": "boolean",
													"description": "Swap Endianness of this index.",
													"default": false
												},
												"signed": {
													"type": "boolean",
													"description": "This index is signed or unsigned integer.",
													"default": false
												}
											}
										}
									}
								}
							}
						}
					}
				}
			},
			"parameters": {
				"type": "array",
				"title": "parameters",
				"description": "List of Startup Parameters to be set before running ethercat instance.",
				"items": {
					"type": "object",
					"title": "startupParameters",
					"required": [
						"index",
						"subindex",
						"size",
						"value"
					],
					"properties": {
						"index": {
							"$ref": "#/$defs/hexadecimal",
							"description": "Startup Parameter's CoE index (in integer or hexadecimal string)."
						},
						"subindex": {
							"$ref": "#/$defs/hexadecimal",
							"description": "Startup Parameter's CoE subindex (in integer or hexadecimal string)."
						},
						"size": {
							"$ref": "#/$defs/hexadecimal",
							"description": "Size in bit (in integer or hexadecimal string)."
						},
						"value": {
							"$ref": "#/$defs/hexadecimal",
							"description": "Startup Parameter's value to be set (in integer or hexadecimal string)."
						}
					},
					"examples": [
						{
							"index": "0x8000",
							"subindex": "0x04",
							"size": 32,
							"value": "0x55"
						}
					]
				}
			}
		},
		"examples": [
			{
				"alias": 0,
				"position": 0,
				"vendor_id": "0x00000002",
				"product_code": "0x044c2c52"
			},
			{
				"alias": 0,
				"position": 1,
				"vendor_id": "0x00000002",
				"product_code": "0x18503052",
				"syncs": [
					{
						"index": 3,
						"watchdog_enabled": false,
						"pdos": [
							{
								"index": "0x1a00",
								"entries": [
									{
										"index": "0x6000",
										"subindex": "0x01",
										"size": 16,
										"add_to_domain": true,
										"swap_endian": true,
										"signed": false
									}
								]
							}
						]
					}
				],
				"parameters": [
					{
						"index": "0x8000",
						"subindex": "0x04",
						"size": 32,
						"value": "0x55"
					}
				]
			}
		]
	}
}
