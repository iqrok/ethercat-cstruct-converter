const { execSync, exec, execFile, spawn } = require('child_process');
const fs = require('fs');
const path = require('path');

const _presets = require(`${__dirname}/presets.lib.js`);

/**
 * @property {string} BIN - main command from etherlab's ethercat master
 * @property {object} cmds - several specific commands to get information from ethercat master
 * @property {string} cmds.slave - command to output currently connected slave(s)
 * @property {string} cmds.cstruct - command to output C struct from slave(s)
 * @property {string} cmds.version - command to output ethercat master version
 * */
const BIN = 'ethercat';
const cmds = {
	slave: `${BIN} slave`,
	cstruct: `${BIN} cstruct`,
	version: `${BIN} version`,
};

class _cstructParser{

	_verbose = 0;
	_SDOs = { objects: {} };

	constructor(){
		// make sure ethercat master is installed
		// throw error if it doesn't exist
		try{
			const check = execSync(cmds.version).toString();
			console.info(check);
		} catch(error){
			console.error(`Error ${error.status}: command '${BIN}' failed`);
			process.exit(error.status);
		}
	}

	/**
	 * Extract value from slave info like vendor id, product code, etc
	 * @private
	 * @param {string} str - output from ethercat slave
	 * @param {string} identifier - id which value would be extracted
	 * @returns {string} value from identifier
	 * */
	_extractSlaveInfoValue(str, identifier, any = false){
		const self = this;

		let value = `0x[0-9a-fA-F]+`;

		if(any) value = `[^\n]+`;

		const regexp = new RegExp(`${identifier}[\:\ ]+(${value})`);
		const match = str.match(regexp);

		return match ? match[1] : undefined;
	}

	/**
	 * Get and cache ethercat sdos output
	 * @private
	 * @param {number} position - slave position
	 * @returns {string} ethercat sdos output
	 * */
	_slaveSDOs(position){
		const self = this;

		if(self._SDOs[position]) return self._SDOs[position];

		try {
			self._SDOs[position] = execSync(`ethercat sdos -p${position}`)
				.toString()
				.trim();

			return self._SDOs[position];
		} catch(error) {
			console.error(`Failed to get SDOs of slave pos ${position}`, error);
			return '';
		}
	}

	/**
	 * Get info of SDO Object from ethercat sdos output
	 * @private
	 * @param {number} position - slave position
	 * @param {string} index - pdo entry index in hex string
	 * @param {string} subindex - pdo entry subindex in hex string
	 * @returns {string[]} array of SDO Object info
	 * */
	_sdoObjectInfo(position, index, subindex){
		const self = this;

		const key = index + ':' + subindex.replace('0x', '');
		const id = `${position}:${key}`;

		if(self._SDOs.objects[id]) return self._SDOs.objects[id];

		try {
			const match = self._slaveSDOs(position)
				.match(new RegExp(`(.*${key}.*)`, 'g'));

			if(!match) {
				self._SDOs.objects[id] = [ '', '', '', '', '' ];
				return self._SDOs.objects[id];
			}

			self._SDOs.objects[id] = match[0].split(',').map(val => val.trim());

			return self._SDOs.objects[id];
		} catch(error) {
			console.error(`Failed to get SDOs of slave pos ${position}`, error);
			return [ '', '', '', '', '' ];
		}
	}

	/**
	 * Determine PDO entry's endiannes. If data type is octet string,
	 * then the endiannes will be swapped.
	 * If failed to get entry's info, will return unsigned.
	 * @private
	 * @param {number} position - slave position
	 * @param {string} index - pdo entry index in hex string
	 * @param {string} subindex - pdo entry subindex in hex string
	 * @returns {boolean} true if PDO entry data type is octet_string
	 * */
	_getSwapEndian(position, index, subindex){
		const self = this;

		try {
			const info = self._sdoObjectInfo(position, index, subindex);
			const type = info[2];

			return Boolean(type && type.match(/octet_string/));
		} catch(error) {
			if(self._verbose >= 1){
				console.error(`Pos ${position} ${index} ${subindex} | Can\'t determine the endiannes. default to false`);
			}

			return false;
		}
	}

	/**
	 * Determine whether PDO entry is signed or unsigned.
	 * If failed to get entry's info, will return unsigned.
	 * @private
	 * @param {number} position - slave position
	 * @param {string} index - pdo entry index in hex string
	 * @param {string} subindex - pdo entry subindex in hex string
	 * @returns {boolean} true if PDO entry is signed
	 * */
	_getIsSigned(position, index, subindex){
		const self = this;

		try {
			const info = self._sdoObjectInfo(position, index, subindex);
			const type = info[2];

			return Boolean(type && type.match(/^int[0-9]+/));
		} catch(error) {
			if(self._verbose >= 1){
				console.error(`Pos ${position} ${index} ${subindex} | Can\'t determine the endiannes. default to false`);
			}

			return false;
		}
	}

	/**
	 * Extract PDO entry from selected cstruct output and convert it into object
	 * @private
	 * @param {number} position - slave position
	 * @param {string} str - selected ethercat cstruct output
	 * @returns {object} PDO entry
	 * */
	_extractPdoEntry(position, str){
		const self = this;

		const matches = str.match(/\{([^\}]+)/m);

		if(!matches) return undefined;

		const arr = matches[1].split(',');
		const index = arr[0].trim();
		const subindex = arr[1].trim();
		const size = Number(arr[2].trim());
		const add_to_domain = parseInt(index, 16) !== 0 ? true : undefined;
		const signed = add_to_domain
			? self._getIsSigned(position, index, subindex)
			: undefined;
		const swap_endian = add_to_domain
			? self._getSwapEndian(position, index, subindex)
			: undefined;

		return { index, subindex, size, add_to_domain, swap_endian, signed };
	}

	/**
	 * Select PDO entries part from ethercat cstruct then convert into object
	 * @private
	 * @param {number} position - slave position
	 * @param {number} start - offset PDO entries
	 * @param {number} length - how many PDO entries will be extracted
	 * @returns {object[]} Array of PDO entries
	 * */
	_getPdoEntries(position, start, length, output){
		const self = this;

		length = +length;
		start = start.match(/[0-9]+$/);

		if(start){
			start = Number(start[0]);
		}

		const pdosEntries = output.match(new RegExp(
				'ec_pdo_entry_info_t\\ [\\w][^{]\+\\{([^;]+)\\}',
				'gm'
			));

		if(!pdosEntries){
			return undefined;
		}

		const arr = pdosEntries[0].split('\n');
		const entries = [];
		for(let idx = 1 + start, counter = 0; counter < length; idx++, counter++){
			const entry = self._extractPdoEntry(position, arr[idx].trim());

			if(entry == undefined){
				continue;
			}

			entries.push(entry);
		}

		return entries;
	}

	/**
	 * get PDO index, offset and length of PDO entry from cstruct output
	 * @private
	 * @param {string} str - PDO entry part from cstruct output
	 * @returns {object} Consist of PDO index, offset, and length
	 * */
	_getPdoIndexBetweenCurly(str){
		const self = this;

		const matches = str.match(/\{([^\}]+)/m);

		if(!matches){
			return undefined;
		}

		const arr = matches[1].split(',');

		return {
			index: arr[0].trim(),
			length: arr[1].trim(),
			start: arr[2].trim(),
		};
	}

	/**
	 * Select PDO infos part from cstruct output
	 * @private
	 * @param {number} position - slave position
	 * @param {number} start - offset Sync Manager entry
	 * @param {number} length - how many PDO infos will be extracted
	 * @param {string} str - full output from ethercat slave
	 * @returns {object[]} Array of PDO infos
	 * */
	_getPdos(position, start, length, output){
		const self = this;

		try{
			length = +length;
			start = +start.match(/[0-9]+$/)[0];

			if(length < 0){
				return undefined;
			}

			const pdosInfo = output.match(new RegExp('ec_pdo_info_t\\ [\\w][^{]\+\\{([^;]+)\\}', 'gm'));

			if(!pdosInfo){
				return undefined;
			}

			const arr = pdosInfo[0].split('\n');
			const pdos = [];
			for(let idx = 1 + start, counter = 0; counter < length; idx++, counter++){
				const pdoInfo = self._getPdoIndexBetweenCurly(arr[idx]);

				if(pdoInfo == undefined){
					continue;
				}

				const { index } = pdoInfo;
				const entries = self._getPdoEntries(
						position,
						pdoInfo.start,
						pdoInfo.length,
						output
					);

				pdos.push({
					index,
					entries,
				});
			}

			return pdos;
		} catch (error) {
			console.error('_getPdos', error);
			return undefined;
		}
	}

	/**
	 * get sync manager info from cstruct output
	 * @private
	 * @param {number} position - slave position
	 * @param {string} str - sync manager part from cstruct output
	 * @param {string} output - full output from ethercat slave
	 * @returns {object} Consist of sync manager index, direction, watchdog, and PDOs
	 * */
	_getSyncIndexBetweenCurly(position, str, output){
		const self = this;

		const matches = str.match(/\{([^\}]+)/m);

		if(!matches){
			return undefined;
		}

		const arr = matches[1].split(',');

		if(arr[3].trim() === 'NULL'){
			return undefined;
		}

		const sync = {
			index: +arr[0].trim(),
			watchdog_enabled: !arr[4].toUpperCase().includes('DISABLE'),
			direction: arr[1].trim().replace('EC_DIR_', '').toLowerCase(),
			pdos: self._getPdos(position, arr[3].trim(), arr[2].trim(), output),
		};

		return sync;
	}

	/**
	 * extract sync manager info from ethercat slave output
	 * then parse every slave with ethercat cstruct
	 * @private
	 * @param {number} position - slave position
	 * @returns {object[]} Array of slave's sync manager info
	 * */
	_extractSyncInfo(position){
		const self = this;

		const output = execSync(`${cmds.cstruct} -p${position}`).toString().trim();
		const syncInfo = output.match(new RegExp('ec_sync_info_t\\ [\\w][^{]\+\\{([^;]+)\\}', 'gm'));

		if(!syncInfo){
			if(self._verbose >= 1){
				console.log(`\n========== CSTRUCT - SLAVE POSITION ${position} ==========`);
				console.log(`No Sync Manager is available for this slave!`);
			}

			return undefined;
		}

		if(self._verbose >= 1){
			console.log(`\n========== CSTRUCT - SLAVE POSITION ${position} ==========`);
			console.log(output);
		}

		const arr = syncInfo[0].split('\n');
		const syncMs = [];
		for(let idx = 1; idx < (arr.length - 2); idx++){
			syncMs.push(self._getSyncIndexBetweenCurly(position, arr[idx], output));
		}

		return syncMs.filter(item => item != null);
	}

	/**
	 * Get configuration as JSON object parsed from ethercat slave & cstruct commands
	 * @param {string} fileout - file path where JSON file will be written
	 * @param {number} verbose - verbosity level
	 * @returns {object[]} Array of slave's configuration
	 * */
	parse(fileout, indentation = undefined, verbose = 0, presetsFile = undefined){
		const self = this;

		if(typeof(fileout) !== 'string'){
			throw Error(`fileout must be a string!`, fileout);
		}

		if(presetsFile !== undefined){
			_presets.setPath(presetsFile);
		}

		self._verbose = Number(verbose);

		const configurations = [];
		const connectedSlaves = execSync(cmds.slave).toString().trim().split('\n');
		const { length } = connectedSlaves;

		if(length <= 1 && connectedSlaves[0] === ''){
			throw Error(`No EtherCAT Slave is detected!`);
		}

		for(let position = 0; position < length; position++){
			const slaveInformation = execSync(`${cmds.slave} -p${position} -v`).toString().trim();

			if(self._verbose >= 1){
				console.log(`\n========== CONFIGURING SLAVE POSITION ${position} ==========`);
				console.log(slaveInformation);
			}

			const id = {
				position,
				vendor_id: self._extractSlaveInfoValue(slaveInformation, '[Vv]endor [Ii]d'),
				product_code: self._extractSlaveInfoValue(slaveInformation, '[Pp]roduct [cC]ode'),
			};

			const presetsCfg = _presets.find(id);

			const obj = {
				alias: 0,
				...id,
				name: self._extractSlaveInfoValue(slaveInformation, '[Oo]rder [Nn]umber', true),
				revision: self._extractSlaveInfoValue(slaveInformation, '[Rr]evision [Nn]umber'),
				serial: self._extractSlaveInfoValue(slaveInformation, '[Ss]erial [Nn]umber'),
				syncs: self._extractSyncInfo(position),
				...presetsCfg,
			};

			configurations.push(obj);

			if(verbose <= 0){
				console.log(`${(100 * (position + 1) / length).toFixed(2)}% (${position + 1}/${length}) : ${obj.name}`);
			}
		}

		if(self._verbose >= 2){
			console.log(`\n========== COMPLETE CONFIGURATION ==========`);
			console.dir(configurations, { depth: null });
		}

		// write JSON file
		fs.promises
			.writeFile(fileout, JSON.stringify(configurations, null, indentation))
			.then(() => console.log(`Saved to '${fileout}'`))
			.catch(error => console.error(`Error at writing file:`, error));

		self._verbose = 0;

		return configurations;
	}
}

module.exports = new _cstructParser();
